#!/usr/bin/evn python
# Demo for smart mplayer on raspberry 3B
# Author: Lin Yonghui <210230@qq.com>

import os
import subprocess
from glob import glob
from ConfigParser import ConfigParser
from mplayer import Player
from bluepy.btle import Scanner, DefaultDelegate, BTLEException

INIFILE = '/home/dst/smplayer.ini'

AudioFileType = '*.mp3'
VideoFileType = '*.mp4'

# Supported beacon address
BEACON_DEVICES = [
    '12:3b:6a:1a:ba:ff',
    '12:3b:6a:1a:ba:f5',
    '12:3b:6a:1a:ba:f4',
    '12:3b:6a:1a:bb:00',
    '12:3b:6a:1a:ea:ff',
    '12:3b:6a:1a:e9:d9',
    '12:3b:6a:1a:eb:00',
    '12:3b:6a:1a:eb:38',
    '12:3b:6a:1a:eb:02',
    '12:3b:6a:1a:e9:d7',
    '12:3b:6a:1a:e9:d6',
    '12:3b:6a:1a:eb:19',
    '12:3b:6a:1a:eb:1a',
    '12:3b:6a:1a:eb:01',
]

# distance for the trigger (meter)
DISTANCE_NEAR = 1.0
DISTANCE_FAR  = 3.0
DISTANCE_VERY_FAR = 5 * DISTANCE_FAR
INDEX_A = 59
INDEX_N = 4.0

# Average amount
AVG_CNT = 5

def rssi_to_distance(rssi):
    # d = 10 ^ ((abs(RSSI) - A) / (10 * n))
    # in which:
    #     A - signal level when the distance is 1 meter
    #     n - fact of environment
    # We use A = 49 and n = 4.0 here
    try:
        power = (abs(rssi) - INDEX_A) / (10 * INDEX_N)
        distance = pow(10, power)
        # print 'Distance = %s (rssi = %s)' % (distance, rssi)
    except:
        # In case the calculation has problems
        print 'Problems when calculating distance from %d' % rssi
        distance = DISTANCE_VERY_FAR
    return distance


class VideoPlayer(object):
    def __init__(self, filename):
        self.filename = filename
        self.player = None

    def start(self):
        print '+++ playing file: %s...' % self.filename
        cmd = 'omxplayer -o both --no-osd %s' % self.filename
        self.player = subprocess.Popen(cmd, shell = True)

    def stop(self):
        print 'unimplemented'

    def is_playing(self):
        return self.player and self.player.poll() is None

class AudioPlayer(object):
    def __init__(self, filename):
        self.mplayer = Player()
        self.filename = filename
        self.mplayer.loadfile(filename)
        # self.mplayer.loop = 1  # Make it play forever to avoid quitting

    def start(self):
        if self.mplayer.paused:
            self.mplayer.loadfile(self.filename)
            self.mplayer.seek(0, 1)
            self.mplayer.pause()
        print '+++ playing file: %s...' % self.filename

    def stop(self):
        if not self.mplayer.paused:
            self.mplayer.pause()
        print '+++ stopped ...'

    def is_playing(self):
        return not self.mplayer.paused


class BeaconDevice(object):
    """One beacon device with its data"""
    def __init__(self, addr):
        self._addr = addr
        self._distance = 0
        self.reset()

    def __repr__(self):
        return '%0.2f' % (self._distance)

    def reset(self):
        self.step = 0
        self.ptr = 0
        self._ds = [0, ] * AVG_CNT

    def update(self, rssi):
        d = rssi_to_distance(rssi)
        if d >= DISTANCE_VERY_FAR:
            return False

        if self.step < AVG_CNT:
            self._ds[self.step] = d
            self._distance = sum(self._ds[:self.step + 1]) / (self.step + 1)
            self.step += 1
            print '<%s>: update distance to %0.2fm' % (
                self._addr, self._distance)
            return True

        self._ds[self.ptr] = d
        self._distance = sum(self._ds) / AVG_CNT
        self.ptr = (self.ptr + 1) % AVG_CNT
        print '[%s]: update distance to %0.2fm' % (self._addr, self._distance)
        return True

    @property
    def distance(self):
        return self._distance

    @property
    def addr(self):
        return self._addr


class DevicesInfo(object):
    """To store global device status"""
    def __init__(self, player):
        self.player = player
        self.devices = {}
        self.playing_devices = {}  # which causes the playing

    def __repr__(self):
        print 'DevicesInfo'


class ScanDelegate(DefaultDelegate):
    def __init__(self, devinfo):
        DefaultDelegate.__init__(self)
        self.player = devinfo.player
        self.devices = devinfo.devices
        self.playing_devices = devinfo.playing_devices

    def r_info(self, addr):
        if self.devices[addr].distance <= DISTANCE_NEAR:
            return 'Near  '
        elif self.devices[addr].distance <= DISTANCE_FAR:
            return 'Middle'
        else:
            return 'Far   '

    def notify_player(self, addr, d):
        #very_near_count = 0  # always start
        #near_count = 0       # keep stared or stopped
        #for ad in self.devices:
        #    if self.devices[ad].distance <= DISTANCE_NEAR:
        #        very_near_count += 1
        #    elif self.devices[ad].distance <= DISTANCE_FAR:
        #        near_count += 1
        #print 'Devices Number: (0 ~ %0.2fm) = %d, (%0.2fm ~ %0.2fm) = %d' % (
        #    DISTANCE_NEAR, very_near_count,
        #    DISTANCE_NEAR, DISTANCE_FAR, near_count)

        print 'Founded Beacon Devices: Near = %0.2fm, Far = %0.2fm' % (
            DISTANCE_NEAR, DISTANCE_FAR)
        for dev in self.devices:
            print '  [%s]: %0.2fm -> %s' % (
                dev, self.devices[dev].distance, self.r_info(dev))

        if self.player.is_playing():
            print '+++ Media is being played...'
            if addr in self.playing_devices:
                if d > DISTANCE_FAR:
                    del self.playing_devices[addr]
            else:
                if d <= DISTANCE_NEAR:
                    self.playing_devices[addr] = d

        else:
            print '+++ Media is stopped...'
            if addr in self.playing_devices:
                if d > DISTANCE_FAR:
                    del self.playing_devices[addr]
            else:
                if d <= DISTANCE_NEAR:
                    self.playing_devices[addr] = d
                    self.player.start()
                    print 'Playing is started...'


    def handleDiscovery(self, dev, isNewDev, isNewData):
        print
        addr = dev.addr.encode('ascii', 'ignore')
        if not addr in BEACON_DEVICES:
            print 'Skip non-beacon device, addr = %s' % addr
            return

        if addr not in self.devices:
            self.devices[addr] = BeaconDevice(addr)
        valid_distance = self.devices[addr].update(dev.rssi)
        if valid_distance:
            self.notify_player(addr, self.devices[addr].distance)

def demo():
    if os.path.exists(INIFILE):
        conf = ConfigParser()
        conf.read(INIFILE)
        autostart = int(conf.get('SmartMPlayer', 'AUTOSTART'))
        if autostart != 1:
            print '+++Warning: Set AUTOSTART to 1 in $HOME/smplayer.ini to run it.'
            return
        global BEACON_DEVICES
        beacons = conf.get('SmartMPlayer', 'BEACON_DEVICES')
        beacons = beacons.replace(' ', '').split(',')
        BEACON_DEVICES = [b for b in beacons if b]
        global DISTANCE_NEAR
        DISTANCE_NEAR = float(conf.get('SmartMPlayer', 'DISTANCE_NEAR'))
        global DISTANCE_FAR
        DISTANCE_FAR = float(conf.get('SmartMPlayer', 'DISTANCE_FAR'))
        global INDEX_A
        INDEX_A = float(conf.get('SmartMPlayer', 'INDEX_A'))
        global INDEX_N
        INDEX_N = float(conf.get('SmartMPlayer', 'INDEX_N'))
        global AVG_CNT
        AVG_CNT = int(conf.get('SmartMPlayer', 'AVG_CNT'))
        print 'Configurations are updated'

    print 'Supported devices:', BEACON_DEVICES

    videofiles = glob(VideoFileType)
    if videofiles:
        print 'Find video file: %s' % videofiles[0]
        mplayer = VideoPlayer(videofiles[0])
    else:
        # audio file must exist if there is no video file
        audiofile = glob(AudioFileType)[0]
        print 'Find audio file: %s' % audiofile
        mplayer = AudioPlayer(audiofile)

    devicesinfo = DevicesInfo(mplayer)
    scanner = Scanner().withDelegate(ScanDelegate(devicesinfo))
    while True:
        try:
            scanner.scan(1.2)
        except BTLEException:
            print '*** BLE error, trying to reset adaptor...',
            subprocess.call("hciconfig hci0 reset", shell = True)
            print 'done.'


if __name__ == '__main__':
    demo()
